# yuhfetch

![preview](src/assets/preview.png)

A simple fetching program written in Python

### Install the requirements:
```bash
pip install -r requirements.txt
```
### To install yuhfetch run:
```bash
make install
```
### Args:
- -v and --version:
prints the version, works only in the yuhfetch's src directory.
